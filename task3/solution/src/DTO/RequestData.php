<?php

namespace solution\src\DTO;

/**
 * Class RequestData
 * @package solution\src\DTO
 */
class RequestData
{
    /** @var string */
    public $httpMethod;

    /** @var string */
    public $url;

    /** @var array */
    public $headers;

    /** @var array */
    public $parameters;
}
