<?php

namespace solution\src\Decorator;

use DateTime;
use Exception;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Log\LoggerInterface;
use solution\src\DTO\RequestData;
use solution\src\Integration\DataProviderInterface;

/**
 * Декоратор принимающий на вход провайдер данных с типом DataProviderInterface
 *
 * Class DecoratorManager
 * @package solution\src\Decorator
 */
// если появится необходимость в новых декораторах,
// то правильно было бы выделить общий функционал в базовый класс декоратора,
// а от него наследовать конкретные, но в данной ситуации нет необходимости множить сущности
class DecoratorManager implements DataProviderInterface
{
    /** @var CacheItemPoolInterface  */
    private $cache;

    /** @var LoggerInterface  */
    private $logger;

    /** @var DataProviderInterface  */
    private $dataProvider;

    /**
     * @param DataProviderInterface $dataProvider
     * @param CacheItemPoolInterface $cache
     * @param LoggerInterface $logger
     */
    public function __construct(DataProviderInterface $dataProvider, CacheItemPoolInterface $cache, LoggerInterface $logger)
    {
        $this->cache = $cache;
        $this->dataProvider = $dataProvider;
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function get(RequestData $input): array
    {
        $result = [];

        try {
            $cacheKey = $this->getCacheKey($input);
            $cacheItem = $this->cache->getItem($cacheKey);

            if ($cacheItem->isHit()) {
                return $cacheItem->get();
            }

            $result = $this->dataProvider->get($input);

            if ($result) {
                $cacheItem
                    ->set($result)
                    ->expiresAt(
                        new DateTime('+1 day')
                    );

                $this->cache->save($cacheItem);
            }

        } catch (Exception $e) {
            $this->logger->critical('Error: ' . $e->getMessage());
        }

        return $result;
    }

    /**
     * Генерирует ключ для кэша
     * Ключ теоретически получается слишком длинным, поэтому на всякий случай решил его в md5() обернуть
     *
     * @param RequestData $input
     *
     * @return string
     */
    protected function getCacheKey(RequestData $input): string
    {
        return md5(json_encode($input));
    }
}