<?php

namespace solution\src\Integration;

use solution\src\DTO\AuthData;
use solution\src\DTO\RequestData;

/**
 * Реализация провайдера
 *
 * Class DataProvider
 * @package solution\src\Integration
 */
class DataProvider implements DataProviderInterface
{
    /** @var AuthData  */
    private $auth;

    /**
     * @param AuthData $auth
     */
    public function __construct(AuthData $auth)
    {
        $this->auth = $auth;
    }

    /**
     * {@inheritdoc}
     */
    public function get(RequestData $request): array
    {
        // returns a response from external service
    }
}