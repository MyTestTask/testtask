<?php

namespace src\Decorator;

use DateTime;
use Exception;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Log\LoggerInterface;
use src\Integration\DataProvider;

// Судя по названию классов тут хотели использовать паттерн Декоратор. Но вместо него используют обычное наследование
class DecoratorManager extends DataProvider
{
    public $cache; // нет phpdoc, лучше сделать private
    public $logger; // нет phpdoc, лучше сделать private

    /**
     * @param string $host
     * @param string $user
     * @param string $password
     * @param CacheItemPoolInterface $cache
     */
    public function __construct($host, $user, $password, CacheItemPoolInterface $cache) // не указан тип для аргументов host, user, password
    {
        parent::__construct($host, $user, $password);
        $this->cache = $cache;
    }

    // нет phpdoc
    // Теоретически getResponse может быть вызван раньше setLogger и мы получим неперехваченное исключение.
    // Лучше всего в конструкторе передавать и логгер тоже.
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger; // в сеттерах лучше всегда возвращать текущий объект, т.е. return $this, чтобы можно было использовать цепочки вызовов
    }

    //  Нет интерфейса от которого происходит наследование phpdoc
    /**
     * {@inheritdoc}
     */
    public function getResponse(array $input) // нет возвращаемого значения :array
    {
        try {
            $cacheKey = $this->getCacheKey($input);
            $cacheItem = $this->cache->getItem($cacheKey);
            if ($cacheItem->isHit()) {
                return $cacheItem->get();
            }

            $result = parent::get($input);

            // возможно лучше добавить проверку на наличие данных в result прежде чем записывать в кэш, но это тонкий случай и зависит от требований.
            $cacheItem
                ->set($result)
                ->expiresAt(
                    // Дату и время можно передать чуть проще: new DateTime('+1 day')
                (new DateTime())->modify('+1 day')
                );

	    // после установки параметров cacheItem нужно сохранять в кэш

            return $result;
        } catch (Exception $e) { // нужно передавать $e->getMessage() в логгер, чтобы понимать какая именно ошибка произошла
            $this->logger->critical('Error');
        }

        return []; // много точек выхода в одном методе может усложнить читаемость, лучше объявить переменную result = [] в начале метода и вынести return $result из конструкции try catch в конец метода вместо return [];

    }

    // нет phpdoc, лучше сделать protected
    public function getCacheKey(array $input) // нет возвращаемого типа string
    {
        return json_encode($input);
    }
}
