SELECT concat(b.name, ' - ', COUNT(ba.author_id)) AS info
  FROM books b
		JOIN books_authors ba
 			ON (ba.book_id = b.id)
 GROUP BY b.name
 HAVING COUNT(ba.author_id) = 3;