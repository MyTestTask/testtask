
**Test Task**
 
Task 1

    Смотреть папку task1 или
    https://www.db-fiddle.com/f/uhhfm1SfZece4Q2fG5iVVT/0
    

Task 2

1. Нет проверки на существования файла (будет выстреливать warning)
2. Возможна перезапись значений в случае одновременного вызова скрипта

Я бы переписал бы вот так

    $count = file_exists('./counter.txt') ? file_get_contents('./counter.txt') : 0;
    file_put_contents("./counter.txt", (int) $count + 1, LOCK_EX);


При частом обращении к файлу могут возникнуть проблемы с чтением/записью, что повлечет за собой запись неправильной информации в файл.
Это случится по нескольким причинам. PHP-процессы выполняются параллельно, не зависят друг от друга, поэтому может случиться ситуация, когда более одного процесса PHP попытаются прочитать информацию из файла, изменить ее и записать.
В данном примере не используется блокировка файла, поэтому, в описанной ситуации, два или более PHP-процессов могут одновременно запуститься и прочтут одну и ту же информацию, одно состояние счетчика, изменят его на +1, и попытаются записать. Но это же неверно. поскольку счетчик должен был измениться на 2 и более (по количеству запущенных процессов), а изменится всего на 1.
Получается, что только последний процесс успешно запишет изменения, т.о. часть информации потеряется.
Поскольку в файле хранится число скачиваний скрипта, то итоговая сумма скачиваний может быть некорректной из-за параллельных процессов чтения/записи файла со счетчиком. Решением данной ситуации может стать блокировка файла в момент чтения/записи, т.е. один процесс блокирует доступ к файлу, пока не закончит изменять в нем информацию. только после этого второму процессу удастся прочитать и перезаписать файл.
Тут снова могут возникнуть проблемы, но уже с доступом. В любом случае, по прошествии времени и в зависимости от частоты запросов, может возникнет очередь на чтение/запись файла, что повлечет за собой увеличение времени работы скрипта.
Если нагрузка на сервер большая, то лучше установить сторонний счетчик например от google на страницу где вызывается этот скрипт, если речь идет об открытом ресурсе.
В противном случае отказаться от хранения в файлах и использовать redis. Redis - он не только работает быстро, поскольку хранит данные в оперативной памяти. но и периодически сохраняет данные на жесткий диск, что повышает его отказоустойчивость в момент прекращения подачи электроэнергии - возможно потеряется часть скачиваний из счетчика, но очень незначительная (зависит от настроек периодичности записи на HDD)


Task 3

    Смотреть папку task3
    ./task3/review - код ревью (старый код с комментариями)
    ./task3/solution - решение


Task 4

    Задача показалась интересной, решил тремя разными способами.
    Смотреть папку task4, запускать на выполнение скипт 
    php ./task4/index.php