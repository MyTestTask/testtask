<?php

trait Valid
{
    /**
     * @param $value
     *
     * @throws InvalidArgumentException
     */
    private static function validate($value) {
        if (preg_match('/\D/', $value)) {
            throw new InvalidArgumentException("The number is invalid: {$value}");
        }
    }

}