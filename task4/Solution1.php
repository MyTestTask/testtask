<?php

class Solution1
{
    use Valid;

    /**
     * Через массивы
     *
     * @param string $a
     * @param string $b
     * @return string
     */
    public static function sum(string $a, string $b): string {
        self::validate($a);
        self::validate($b);

        $arrayA = str_split($a);
        $arrayB = str_split($b);
        $arrayC = [];
        $max    = max(count($arrayA), count($arrayB));
        $arrayA = array_reverse($arrayA);
        $arrayB = array_reverse($arrayB);
        for ($i = 0; $i < $max; $i++) {
            $arrayC[] = ($arrayA[$i] ?? 0) + ($arrayB[$i] ?? 0);
        }
        $arrayResult = [];
        $tmp         = 0;
        foreach ($arrayC as $item) {
            $item = (string) ((int) $item + $tmp);
            if (strlen($item) === 1) {
                $arrayResult[] = $item;
                $tmp           = 0;
            } else {
                $tmp           = $item[0];
                $arrayResult[] = $item[1];
            }
        }

        if ($tmp) {
            $arrayResult[] = $tmp;
        }

        $stringResult = array_reverse($arrayResult);

        return implode('', $stringResult);
    }
}
